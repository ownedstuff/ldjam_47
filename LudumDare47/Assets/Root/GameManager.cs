﻿using Assets.Root.Audio;
using Assets.Root.Hud;
using Assets.Root.Player;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Root
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private PlayerData playerData;

        [Header("HUD")]
        [SerializeField] private MainMenuHudController mainMenuHud;
        [SerializeField] private GameHudController gameHud;

        [SerializeField] private ShopHudController shopHud;
        [SerializeField] private PipeManager pipeManager;

        [SerializeField] private CameraLock cameraManager;

        private bool stuckyLoopEngineReady = false;

        public void InitStuckyLoopEngine()
        {
            OpenMainMenu();
            stuckyLoopEngineReady = true;
        }

        void Start()
        {
        }

        private void Update()
        {
            if (!stuckyLoopEngineReady) return;

            if (playerData.Health <= 0 || Input.GetKeyDown(KeyCode.K))
            {
                TransitionToShop();
            }
        }

        private void OpenMainMenu()
        {
            playerData.RemoveFromGame();
            playerData.ResetPlayerTempValues();

            gameHud.Hide();
            shopHud.Hide();
            mainMenuHud.Show();
            cameraManager.ShowMenuCam();
        }

        private void TransitionToShop()
        {
            var newPoints = (playerData.DistanceTraveled / 100) + playerData.Coins * 10;
            playerData.GivePoints((int)newPoints);

            playerData.RemoveFromGame();
            playerData.ResetPlayerTempValues();

            gameHud.Hide();
            shopHud.Show();
            cameraManager.ShowMenuCam();
        }

        public void StartNewGame()
        {
            PlayAgain();
        }

        public void PlayAgain()
        {
            mainMenuHud.Hide();
            shopHud.Hide();

            playerData.ResetPlayerTempValues();
            gameHud.Show();

            playerData.PutInGame();
            pipeManager.ResetLevels();

            cameraManager.ShowPlayerCam();
        }
    }
}