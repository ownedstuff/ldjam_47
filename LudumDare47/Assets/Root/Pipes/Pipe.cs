﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pipe : MonoBehaviour , IPoolable
{
    public const string Tag = "Pipe";

    [SerializeField] private float BaseRotationSpeed = 5;
    [SerializeField] private Transform spinner;
    [SerializeField] private GameObject coinPrefab;
    [SerializeField] private float radiusCoinPlacingMin = 15f;
    [SerializeField] private float radiusCoinPlacingMax = 22f;

    private List<CoinPowerUp> coinPowerUps = new List<CoinPowerUp>();

    public int RotationSpeedMultiplier { get; set; }

    public int Size;

    void Update()
    {
        var rotation = new Vector3(0, 0, 1);
        spinner.Rotate(rotation, RotationSpeedMultiplier * BaseRotationSpeed * Time.deltaTime);
    }

    public void GenerateNewCoins()
    {
        for (int zPos = 0; zPos < Size; zPos += 10)
        {
            GenerateCoin(zPos);
        }
    }

    private void GenerateCoin(int zPos)
    {
        var maxPossibleCoinsCount = Size / 10;
        if (coinPowerUps.Count >= maxPossibleCoinsCount)
        {
            return;
        }

        var coinObj = Instantiate(coinPrefab, spinner);
        var coin = coinObj.GetComponent<CoinPowerUp>();

        coinPowerUps.Add(coin);
        coin.OnCoinDestroy += () => coinPowerUps.Remove(coin);

        var pos = coinObj.transform.localPosition;

        var radAngle = UnityEngine.Random.Range(0, 360) * Mathf.Deg2Rad;
        var radiusSize = UnityEngine.Random.Range(radiusCoinPlacingMin, radiusCoinPlacingMax);

        var spawnX = Mathf.Cos(radAngle) * radiusSize;
        var spawnY = Mathf.Sin(radAngle) * radiusSize;

        pos.z = zPos;
        pos.x = spawnX;
        pos.y = spawnY;
        coinObj.transform.localPosition = pos;
    }

    public void OnRetrieve()
    {
        //Do nothing for now
    }
}
