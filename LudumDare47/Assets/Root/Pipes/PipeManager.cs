﻿using Assets.Root.Player;
using Assets.Root.Pooler;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PipeManager : MonoBehaviour
{
    [Header("References")]
    [SerializeField] private ObjectPoolerScript pooler;
    [SerializeField] private PlayerData playerData;
    [SerializeField] private PlayerMovement playerMovement;

    [Header("Properties")]
    [SerializeField] private int targetFullPipeLength;
    [SerializeField] private int basePipeSpeed;
    [SerializeField] private int desactivateCameraBufferSize = 15;
    [SerializeField] private List<Level> levels;

    private List<Pipe> chainOfPipes = new List<Pipe>();
    private Poolables lastPipeTypeSpawn = Poolables.BasicPipe;

    private float actualPipeSpeed;
    private int fullPipeLength = 0;
    private int currentLevelDistance = 0;

    void Start()
    {
        RefillChainOfPipes();
    }

    void Update()
    {
        UpdateActivePipePositions();
        UpdateActualSpeed();
        UpdateDistanceTraveled();

        var pipe = chainOfPipes.FirstOrDefault();
        if (pipe == null) return;

        var pipeIsBehindPlayer = pipe.transform.position.z < -pipe.Size - desactivateCameraBufferSize;

        if (pipeIsBehindPlayer)
        {
            DeactivatePipe(pipe);
            RefillChainOfPipes();
        }
    }

    public void ResetLevels()
    {
        chainOfPipes.ForEach(p=> p.gameObject.SetActive(false));
        chainOfPipes = new List<Pipe>();
        currentLevelDistance = 0;
        fullPipeLength = 0;

        RefillChainOfPipes();
    }
    // formule: -----------------
    // L'image d'un point (px, py) après une homothetie avec le centre (cx, cy) et ratio R 
    // est egale à (cx + R(px − cx), cy + R(py − cy)).
    // --------------------------
    // en gros, ça transforme un chifre de -1 à 1 vers un chiffre de 0.5 à 2
    // dans ce cas ci vu que c'est juste un chiffre et non un point
    // dans un plan, on peut juste considérer la composante y
    // les valeurs du ratio et du centre ont été calculé d'avance
    private float MapPlayerHasteToSpeedIncrease(float py)
    {
        float cy = 5, R = 0.75f;

        return cy + R * (py - cy);
    }

    private void UpdateActualSpeed()
    {
        var wantedHaste = MapPlayerHasteToSpeedIncrease(playerMovement.CurrentHaste); ;

        actualPipeSpeed = (basePipeSpeed * wantedHaste) * playerData.CurrentSpeedMultiplier;
    }

    private void UpdateDistanceTraveled()
    {
        playerData.AddDistanceTraveled(actualPipeSpeed * Time.deltaTime);
    }

    private void DeactivatePipe(Pipe pipe)
    {
        pipe.gameObject.SetActive(false);
        fullPipeLength -= pipe.Size;

        chainOfPipes.Remove(pipe);
    }

    private void UpdateActivePipePositions()
    {
        foreach (var pipe in chainOfPipes)
        {
            if (!pipe.gameObject.activeInHierarchy) continue;
            UpdatePipePosition(pipe);
        }
    }

    private void UpdatePipePosition(Pipe pipe)
    {
        var position = pipe.transform.position;
        var newZ = position.z - actualPipeSpeed * Time.deltaTime;
        pipe.transform.position = new Vector3(0, 0, newZ);
    }

    private void RefillChainOfPipes()
    {
        while (fullPipeLength < targetFullPipeLength)
        {
            var pipe = GetRandomPipe();

            pipe.GenerateNewCoins();
            PlacePipeAtTheEnd(pipe);
            AddPipeToChain(pipe);

            fullPipeLength += pipe.Size;
        }
    }

    private Pipe GetRandomPipe()
    {
        GameObject pipeGO = null;
        Level level = GetLevel();

        if (playerData.IsInGame && level.distance > currentLevelDistance)
        {
            pipeGO = pooler.Retrieve(Poolables.LevelCrossPipe);
            currentLevelDistance = level.distance;
        }
        else 
        {
            print("Level: " + level.ToString());
            var pipeToSpawn = GetPipeFromLevelRecursively(level);
            lastPipeTypeSpawn = pipeToSpawn;

            pipeGO = pooler.Retrieve(pipeToSpawn);
        }

        return pipeGO.GetComponent<Pipe>();
    }

    private Level GetLevel()
    {
        return !playerData.IsInGame ? 
            levels.Last() : 
            levels.Last(l => l.distance <= playerData.DistanceTraveled);
    }

    private Poolables GetPipeFromLevelRecursively(Level level)
    {
        var random = Random.Range(0, level.TotalWeight);
        var pipeToSpawn = level.WeightedPipeList().ElementAt(random);

        if (!pipeToSpawn.CanRepeat && pipeToSpawn.Pipe == lastPipeTypeSpawn)
        {
            return GetPipeFromLevelRecursively(level);
        }

        return pipeToSpawn.Pipe;
    }

    private void PlacePipeAtTheEnd(Pipe pipe)
    {
        float offset = GetLastPipeOffset();
        pipe.transform.position = new Vector3(0, 0, offset);
    }

    private void AddPipeToChain(Pipe pipe)
    {
        pipe.gameObject.SetActive(true);
        chainOfPipes.Add(pipe);

        var rotation = Random.Range(2, 5);
        var leftRight = Random.Range(0, 2) == 1 ? 1 : -1;
        rotation = rotation * leftRight;

        pipe.RotationSpeedMultiplier = rotation;
    }

    private float GetLastPipeOffset()
    {
        var lastPipe = chainOfPipes.LastOrDefault();
        if (lastPipe == null) return 0f;

        float lastPipePos = lastPipe.transform.position.z;
        float lastPipeSize = lastPipe.Size;

        return lastPipePos + lastPipeSize;
    }
}
