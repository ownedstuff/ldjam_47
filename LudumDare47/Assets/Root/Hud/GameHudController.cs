﻿using Assets.Root.Audio;
using Assets.Root.Player;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Assets.Root.Hud
{
    public class GameHudController : MonoBehaviour
    {
        [SerializeField] private PlayerData playerData;
        [SerializeField] private TextMeshProUGUI textMeshPro;
        [SerializeField] private AudioManager audioManager;

        void Update()
        {
            textMeshPro.SetText($"Heart: {playerData.Health:0}/{playerData.MaxHealth} \nDistance: {playerData.DistanceTraveled:0} \nMax Distance: {playerData.LongestDistanceTraveled:0} \nCoins: {playerData.Coins}\n {playerData.CurrentSpeedMultiplier}");
        }

        public void Hide()
        {
            audioManager.StopNow(KnownedTrackCombinaisions.Song2);
            gameObject.SetActive(false);
        }

        public void Show()
        {
            audioManager.PlayNow(KnownedTrackCombinaisions.Song2);
            gameObject.SetActive(true);
        }
    }
}