﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using static UnityEngine.UI.Button;

namespace Assets.Root.Hud.ShopComponents
{
    public class PowerUpButton : MonoBehaviour
    {
        [SerializeField] private RectTransform rectTransform;
        [SerializeField] private Button clickableButton;
        [SerializeField] private Image imageBackground;
        [SerializeField] private TextMeshProUGUI titleTextMesh;
        [SerializeField] private TextMeshProUGUI coinsTextMesh;

        private int powerUpId;

        private bool isShownAsEnabeled = true;

        private Action<int> onPowerUpClickedAction;

        public void SetId(int id)
        {
            powerUpId = id;
            clickableButton.onClick.RemoveAllListeners();
            clickableButton.onClick.AddListener(PowerUpClicked);
        }

        private void PowerUpClicked()
        {
            onPowerUpClickedAction?.Invoke(powerUpId);
        }

        public void SetShowEnabled(bool showEnabeled)
        {
            isShownAsEnabeled = showEnabeled;
            imageBackground.color = showEnabeled ? Color.white : Color.grey;
        }

        public void SetTitle(string title)
        {
            titleTextMesh.SetText($"{title}");
        }

        public void SetCost(int pointCost)
        {
            coinsTextMesh.SetText($"{pointCost} points");
        }

        public void AddClickListener(Action<int> onPowerUpClicked)
        {
            onPowerUpClickedAction = onPowerUpClicked;
        }

        public float GetSize()
        {
            return rectTransform.rect.width;
        }
    }
}