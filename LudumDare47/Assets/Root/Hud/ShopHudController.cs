﻿using Assets.Root.Audio;
using Assets.Root.Hud.ShopComponents;
using Assets.Root.Player;
using Assets.Root.PowerUp;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Root.Hud
{
    public class ShopHudController : MonoBehaviour
    {
        [Header("UI")]
        [SerializeField] private TextMeshProUGUI coinsText;
        [SerializeField] private TextMeshProUGUI shopMessageText;
        [SerializeField] private RectTransform powerUpsCanvas;
        [SerializeField] private PowerUpButton powerUpButtonPrefab;

        [Header("Data")]
        [SerializeField] private PlayerData playerData;
        [SerializeField] private PowerUpsDefinition powerUpsDefinition;

        [Header("Managers")]
        [SerializeField] private GameManager gameManager;
        [SerializeField] private AudioManager audioManager;

        private const float defaultButtonPadding = 20;

        private Dictionary<int, PowerUpButton> powerUpButtons = new Dictionary<int, PowerUpButton>();

        void Start()
        {
            shopMessageText.SetText("");
            PlayMenuSong();
            GeneratePowerUpButtons();
        }

        void Update()
        {
            coinsText.SetText($"{playerData.Points} points");
        }

        private void GeneratePowerUpButtons()
        {
            bool isFirstButtonToPlace = true;

            var padding = 20f;
            float currentLineX = padding;
            float currentColumnY = 0;
            var definitions = powerUpsDefinition.GetAllPowerUpsDefinition();
            for (int i = 0; i < definitions.Length; i++)
            {
                var definition = definitions[i];
                var powerUpButton = Instantiate(powerUpButtonPrefab, powerUpsCanvas.transform);
                powerUpButtons[definition.Id] = powerUpButton;
                var size = powerUpButton.GetSize();

                if (isFirstButtonToPlace)
                {
                    padding = CalculatePading(powerUpsCanvas.rect.width, size);
                    currentLineX = padding;
                    isFirstButtonToPlace = false;
                }

                PlaceButtonInGrid(powerUpButton, currentLineX, currentColumnY);

                InitializeButtonProperties(powerUpButton, definition);

                currentLineX = UpdateCurrentLineXForNextButton(padding, currentLineX, size);
                if (ShouldWrapNextLine(padding, currentLineX, size))
                {
                    currentColumnY -= size + padding;
                    currentLineX = padding;
                }
            }
        }

        private bool ShouldWrapNextLine(float padding, float currentLineX, float size)
        {
            return currentLineX + size + padding > powerUpsCanvas.rect.width;
        }

        private static float UpdateCurrentLineXForNextButton(float padding, float currentLineX, float size)
        {
            currentLineX += size + padding * 2;
            return currentLineX;
        }

        private void InitializeButtonProperties(PowerUpButton powerUpButton, PowerUpDefinition definition)
        {
            bool hasPowerUp = playerData.HasPowerUp(definition.Id);
            powerUpButton.SetShowEnabled(!hasPowerUp);

            powerUpButton.SetId(definition.Id);
            powerUpButton.SetTitle(definition.DisplayFormat(withNewLine: true));
            powerUpButton.SetCost(definition.PointsCost);
            powerUpButton.AddClickListener(TryToBuyPowerUp);
        }

        private void TryToBuyPowerUp(int powerUpId)
        {
            var powerUp = powerUpsDefinition.GetPowerUpsDefinition(powerUpId);
            if (playerData.HasPowerUp(powerUpId))
            {
                shopMessageText.SetText($"You already have {powerUp.DisplayFormat()}");
                return;
            }

            if (powerUp.PointsCost > playerData.Points)
            {
                shopMessageText.SetText($"Not enought points to buy {powerUp.DisplayFormat()}");
                return;
            }

            if (playerData.Buy(powerUp))
            {
                var button = powerUpButtons[powerUp.Id];
                button.SetShowEnabled(false);
            }
            else
            {
                shopMessageText.SetText($"Unable to buy {powerUp.DisplayFormat()}");
                return;
            }
        }

        private float CalculatePading(float canvaWidth, float size)
        {
            float ajustedSize = size + defaultButtonPadding * 2;
            int sections = (int)(canvaWidth / ajustedSize);
            float sectionsSize = canvaWidth / sections;
            float newPadding = sectionsSize - size;
            return newPadding;
        }

        private void PlaceButtonInGrid(PowerUpButton powerUpButton, float x, float y)
        {
            var wtfFreakingCanvasLocalOrNonLocalTransformParentDontWork_Y = y + powerUpsCanvas.rect.height + powerUpsCanvas.rect.y;
            var canvasAndX = x - powerUpsCanvas.rect.width / 2;
            powerUpButton.transform.localPosition = new Vector3(canvasAndX, wtfFreakingCanvasLocalOrNonLocalTransformParentDontWork_Y);
        }

        public void PlayAgain()
        {
            gameManager.PlayAgain();
        }

        public void Hide()
        {
            gameObject.SetActive(false);
            StopMenuSong();
        }

        public void Show()
        {
            gameObject.SetActive(true);
            PlayMenuSong();
        }

        private void PlayMenuSong()
        {
            audioManager.PlayNow(KnownedTrackCombinaisions.Song2MainMenu);
        }

        private void StopMenuSong()
        {
            audioManager.StopNow(KnownedTrackCombinaisions.Song2MainMenu);
        }
    }
}