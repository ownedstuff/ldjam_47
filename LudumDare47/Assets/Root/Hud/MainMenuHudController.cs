﻿using Assets.Root.Audio;
using Assets.Root.Player;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Root.Hud
{
    public class MainMenuHudController : MonoBehaviour
    {
        [SerializeField] private GameManager gameManager;
        [SerializeField] private AudioManager audioManager;

        void Start()
        {
            gameManager.InitStuckyLoopEngine();
        }

        void Update()
        {
        }

        public void StartGame()
        {
            gameManager.StartNewGame();
        }

        public void QuitGame()
        {
#if UNITY_EDITOR

            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }

        public void Show()
        {
            gameObject.SetActive(true);
            PlayMenuSong();
        }

        private void PlayMenuSong()
        {
            audioManager.PlayNow(KnownedTrackCombinaisions.Song2MainMenu);
        }

        public void Hide()
        {
            audioManager.StopNow(KnownedTrackCombinaisions.Song2MainMenu);
            gameObject.SetActive(false);
        }
    }
}