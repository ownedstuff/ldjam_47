﻿using Assets.Root.Player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Root.PowerUp.HealthPowerUp
{
    public class HealthPowerUpController : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == PlayerData.Tag)
            {
                var playerData = other.GetComponent<PlayerData>();
                playerData.HealthOne();

                gameObject.SetActive(false);
            }
        }
    }
}