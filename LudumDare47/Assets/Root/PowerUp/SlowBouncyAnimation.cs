﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Root.PowerUp
{
    public class SlowBouncyAnimation : MonoBehaviour
    {
        public event System.Action OnCoinDestroy;

        [Header("Animation")]
        [SerializeField] private float oscillationSpeed = Mathf.PI;
        [SerializeField] private float rotationSpeed = 180;

        private float currentTimeForY;

        private float currentRotation;

        private Vector3 initialPosition;

        private void Start()
        {
            initialPosition = transform.localPosition;
        }

        private void FixedUpdate()
        {
            UpdateRotation();
            UpdateOscillation();
        }

        private void UpdateRotation()
        {
            IncrementRotation();

            var angles = transform.localRotation.eulerAngles;
            angles.y = currentRotation;
            transform.localRotation = Quaternion.Euler(angles);
        }

        private void IncrementRotation()
        {
            currentRotation += rotationSpeed * Time.deltaTime;
        }

        private void UpdateOscillation()
        {
            IncrementOscillation();

            var oscillationYOffset = Mathf.Sin(currentTimeForY) * 0.5f;
            transform.localPosition = new Vector3(transform.localPosition.x, initialPosition.y + oscillationYOffset, transform.localPosition.z);
        }

        private void IncrementOscillation()
        {
            currentTimeForY += oscillationSpeed * Time.deltaTime;
            if (currentTimeForY > oscillationSpeed)
            {
                currentTimeForY -= oscillationSpeed;
            }
        }
    }
}