﻿using Assets.Root.Player;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CoinPowerUp : MonoBehaviour,  IPoolable
{
    public event System.Action OnCoinDestroy;

    [SerializeField] private Transform target;

    [Header("Mega coin")]
    [SerializeField] private Material megaCoinMaterial;
    [SerializeField] private Renderer[] megaCoinsRenderersToChange;
    [SerializeField] private bool isMegaCoin = false;

    private void Start()
    {

        if (isMegaCoin && megaCoinsRenderersToChange.Any())
        {
            foreach (var renderer in megaCoinsRenderersToChange)
            {
                renderer.material = megaCoinMaterial;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == PlayerData.Tag)
        {
            var playerData = other.GetComponent<PlayerData>();
            GiveCoin(playerData);

            target.gameObject.SetActive(false);

            var particleSystem = GetComponentInChildren<ParticleSystem>();
            particleSystem.Play();

            StartCoroutine(DelayedDestroyCoin(particleSystem.main.duration));
        }
    }

    private void GiveCoin(PlayerData playerData)
    {
        if (isMegaCoin)
        {
            playerData.GiveMegaCoin();
        }
        else
        {
            playerData.GiveCoin();
        }
    }

    IEnumerator DelayedDestroyCoin(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);

        OnCoinDestroy?.Invoke();
        Destroy(gameObject);
    }
    public void OnRetrieve()
    {
        //do nothing for now
    }
}
