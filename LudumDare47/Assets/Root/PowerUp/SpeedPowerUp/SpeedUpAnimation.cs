﻿using Assets.Root.Player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedUpAnimation : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == PlayerData.Tag)
        {
            var playerData = other.GetComponent<PlayerData>();
            playerData.ActivateSpeedPowerUp();
            gameObject.SetActive(false);
        }
    }
}
