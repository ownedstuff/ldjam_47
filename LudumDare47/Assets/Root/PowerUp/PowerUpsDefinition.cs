﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Root.PowerUp
{
    public class PowerUpsDefinition : MonoBehaviour
    {
        private PowerUpDefinition[] PowerUps;

        private int NextId { get; set; }

        public PowerUpsDefinition()
        {
            PowerUps = new PowerUpDefinition[]
            {
                CreatePowerUp("Speed", level: 1, 50, incrementBaseSpeedMultiplier: 0.5f),
                CreatePowerUp("Speed", level: 2, 250, incrementBaseSpeedMultiplier: 1f),
                CreatePowerUp("Speed", level: 3, 1000, incrementBaseSpeedMultiplier: 2f),

                CreatePowerUp("Jumps", level: 1, 50, incrementBaseJumpCount: 1),
                CreatePowerUp("Jumps", level: 2, 250, incrementBaseJumpCount: 2),
                CreatePowerUp("Jumps", level: 3, 1000, incrementBaseJumpCount: 4),

                CreatePowerUp("Max heart", level: 1, 50, incrementBaseMaxHeart: 1),
                CreatePowerUp("Max heart", level: 2, 250, incrementBaseMaxHeart: 2),
                CreatePowerUp("Max heart", level: 3, 1000, incrementBaseMaxHeart: 4),
            };
        }

        public PowerUpDefinition GetPowerUpsDefinition(int id)
        {
            return PowerUps[id];
        }

        public PowerUpDefinition[] GetAllPowerUpsDefinition()
        {
            return PowerUps;
        }

        private PowerUpDefinition CreatePowerUp(string name, int level, int cost, int incrementBaseJumpCount = 0, int incrementBaseMaxHeart = 0, float incrementBaseSpeedMultiplier = 0)
        {
            return new PowerUpDefinition()
            {
                Id = NextId++,
                Name = name,
                PointsCost = cost,
                Level = level,
                IncrementBaseJumpCount = incrementBaseJumpCount,
                IncrementBaseMaxHeart = incrementBaseMaxHeart,
                IncrementBaseSpeedMultiplier = incrementBaseSpeedMultiplier,
            };
        }
    }

    public class PowerUpDefinition
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int PointsCost { get; set; }

        public int Level { get; set; }

        public int IncrementBaseJumpCount { get; set; }

        public int IncrementBaseMaxHeart { get; set; }

        public float IncrementBaseSpeedMultiplier { get; set; }

        public string DisplayFormat()
        {
            return DisplayFormat(withNewLine: false);
        }

        public string DisplayFormat(bool withNewLine)
        {
            var newLine = withNewLine ? "\n" : string.Empty;
            return $"{Name} {newLine}Lvl {Level}";
        }
    }
}