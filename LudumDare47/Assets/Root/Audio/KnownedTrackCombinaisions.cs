﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Root.Audio
{
    public enum KnownedTrackCombinaisions
    {
        None,
        Jump1,
        Acceleration,
        Deceleration,
        SpeedAmbiantSound,
        Song2,
        Song2MainMenu,
        Coin,
        Hurt
    }
}
