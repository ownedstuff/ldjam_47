﻿using System;
using UnityEngine;

namespace Assets.Root.Audio
{
    [Serializable]
    public class AudioTrack
    {
        public string name;

        public AudioClip clip;

        [Range(0f, 1f)]
        public float volume;

        [Range(.1f, 3f)]
        public float pitch;

        public bool loop;

        [HideInInspector]
        public AudioSource source;

        public bool playing;
    }
}
