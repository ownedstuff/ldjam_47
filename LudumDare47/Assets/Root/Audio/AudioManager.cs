﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Root.Audio
{
    public class AudioManager : MonoBehaviour
    {
        public AudioTrack[] tracks;

        public static AudioManager instance;

        private float lastTimeOfBaseMusic;
        private float loopLenght;

        private double timeSinceLastPlay;

        private KnownedTrackCombinaisions currentTrackCombinaision;

        private Dictionary<KnownedTrackCombinaisions, string[]> trackCombinaisonDefinition = new Dictionary<KnownedTrackCombinaisions, string[]>()
        {
            { KnownedTrackCombinaisions.Jump1, new string[] { "jump_1" } },
            { KnownedTrackCombinaisions.Acceleration, new string[] { "acceleration" } },
            { KnownedTrackCombinaisions.Deceleration, new string[] { "deceleration" } },
            { KnownedTrackCombinaisions.SpeedAmbiantSound, new string[] { "speed_ambiant_sound" } },
            { KnownedTrackCombinaisions.Song2, new string[] { "song_2" } },
            { KnownedTrackCombinaisions.Song2MainMenu, new string[] { "song_2_main_menu" } },
            { KnownedTrackCombinaisions.Hurt, new string[] { "hurt" } },
            { KnownedTrackCombinaisions.Coin, new string[] { "coin" } }
        };

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(gameObject);
                return;
            }

            DontDestroyOnLoad(gameObject);
            UnityEngine.Random.InitState(DateTime.Now.Millisecond);

            foreach (var t in tracks)
            {
                t.source = gameObject.AddComponent<AudioSource>();
                t.source.clip = t.clip;

                t.source.volume = t.volume;
                t.source.pitch = t.pitch;
                t.source.loop = t.loop;
            }
        }

        [Obsolete("Use PlayNow instead")]
        public void PlayTrack(KnownedTrackCombinaisions track)
        {
            if (currentTrackCombinaision == track)
            {
                return;
            }

            if (!trackCombinaisonDefinition.ContainsKey(track))
            {
                return;
            }

            var firstTrack = trackCombinaisonDefinition[track].FirstOrDefault();
            if (firstTrack == null)
            {
                return;
            }

            // Stop all sounds
            Stop(tracks.Select(s => s.name).ToArray());

            var trackToLoop = Play(firstTrack).First();

            currentTrackCombinaision = track;
            timeSinceLastPlay = 0;

            // all other musics are 1 loop lenght
            loopLenght = trackToLoop.clip.length;
        }

        public void ChangePlayingTrackPitch(KnownedTrackCombinaisions track, float pitch)
        {
            if (!trackCombinaisonDefinition.ContainsKey(track))
            {
                return;
            }

            var trackNamesToChange = trackCombinaisonDefinition[track];
            foreach (var trackNameToChange in trackNamesToChange)
            {
                var trackToChange = tracks.FirstOrDefault(t => t.name == trackNameToChange);
                if (trackToChange == null)
                {
                    continue;
                }

                trackToChange.source.pitch = pitch;
            }
        }

        public void PlayNow(KnownedTrackCombinaisions track)
        {
            if (!trackCombinaisonDefinition.ContainsKey(track))
            {
                return;
            }

            var firstTrack = trackCombinaisonDefinition[track].FirstOrDefault();
            Play(firstTrack).First();
        }

        public void StopNow(KnownedTrackCombinaisions track)
        {
            if (!trackCombinaisonDefinition.ContainsKey(track))
            {
                return;
            }

            var firstTrack = trackCombinaisonDefinition[track].FirstOrDefault();
            Stop(firstTrack);
        }

        private AudioTrack[] Play(params string[] names)
        {
            var tracksToPlay = tracks.Where(s => names.Contains(s.name));
            if (!tracksToPlay.Any()) return new AudioTrack[0];

            foreach (var trackToPlay in tracksToPlay)
            {
                trackToPlay.source.Play();
                trackToPlay.playing = true;
            }
            return tracksToPlay.ToArray();
        }

        private void Stop(params string[] names)
        {
            var tracksToStop = tracks.Where(s => names.Contains(s.name));
            if (!tracksToStop.Any()) return;

            foreach (var trackToStop in tracksToStop)
            {
                trackToStop.source.Stop();
                trackToStop.playing = false;
            }
        }

        private void Update()
        {
            if (currentTrackCombinaision != KnownedTrackCombinaisions.None)
            {
                timeSinceLastPlay += Time.deltaTime;
            }
            else
            {
                timeSinceLastPlay = 0;
                return;
            }

            var firstTrackName = trackCombinaisonDefinition[currentTrackCombinaision][0];
            var baseSound = tracks.Where(s => s.name == firstTrackName).FirstOrDefault();

            var possibleTrackNames = trackCombinaisonDefinition[currentTrackCombinaision];

            // When base music just looped to start, add or remove a music
            var loopTime = baseSound.source.time % loopLenght;
            if (lastTimeOfBaseMusic > loopTime)
            {
                var possibleTracks = tracks.Where(p => possibleTrackNames.Contains(p.name));

                var numberOfPlayingTracks = possibleTracks.Where(s => s.playing).Count();

                // should reduce
                if (numberOfPlayingTracks == possibleTrackNames.Length && possibleTrackNames.Length > 1)
                {
                    var playingTracks = possibleTracks.Where(p => p.playing).ToArray();
                    var indexToStop = UnityEngine.Random.Range(1, playingTracks.Length);
                    Stop(playingTracks[indexToStop].name);
                }
                else
                {
                    var nextPlayingTrack = possibleTracks.Where(p => !p.playing).FirstOrDefault();
                    if (nextPlayingTrack != null)
                    {
                        var newPlayingSounds = Play(nextPlayingTrack.name);
                        foreach (var newPlayingSound in newPlayingSounds)
                        {
                            newPlayingSound.source.timeSamples = baseSound.source.timeSamples;
                        }
                    }
                }
            }

            lastTimeOfBaseMusic = loopTime;
        }
    }
}
