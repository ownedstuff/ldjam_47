﻿
namespace Assets.Root.Pooler
{
    public enum Poolables
    {
        Coin,
        BasicPipe,
        BarrierPipe,
        LevelCrossPipe,
        SingleFanPipe,
        DoubleFanPipe,
        SingleBarsPipe,
        DoubleBarsPipe,
        FloatingPlatformPipe,
        ParcourPipe,
        LaserLoopPipe,
        WildLaserPipe
    }
}
