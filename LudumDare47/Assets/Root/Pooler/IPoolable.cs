﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPoolable{

    /// <summary>
    /// OnPooled is called by the pooler right before the object is reactivated
    /// </summary>
    void OnRetrieve();
}
