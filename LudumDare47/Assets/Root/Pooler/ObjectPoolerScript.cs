﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;
using Assets.Root.Pooler;

public class ObjectPoolerScript : MonoBehaviour {

	public List<ObjectPoolData> ObjectPoolsData = new List<ObjectPoolData>();
	public static ObjectPoolerScript instance;
	private Dictionary<Poolables, List<GameObject>> ObjectPools = new Dictionary<Poolables, List<GameObject>>();

	void Awake () {
		instance = this;
        ObjectPoolsData.ForEach(opd => CreatePool(opd));
    }

    private void CreatePool(ObjectPoolData pooledObjectData)
    {
        List<GameObject> pool = new List<GameObject>();
        for (int j = 0; j < pooledObjectData.poolSize; j++)
        {
            GameObject obj = Instantiate(pooledObjectData.prefab);
            obj.SetActive(false);
            pool.Add(obj);
        }
        ObjectPools.Add(pooledObjectData.name, pool);
    }

    /// <summary>
    /// returns the first inactive item in the list inside the dictionary @pooledObjects[which]
    /// if there is no items available and the list is expandable it will add a new item in the list and return it
    /// </summary>
    /// <param name="objectToRetrieve" type="string">string that tells the function which item to retrieve</param>
    /// <returns></returns>
    public GameObject Retrieve(Poolables objectToRetrieve){
        var obj = GetObject(objectToRetrieve);
        if(obj == null)
        {
            obj = CreateNewObject(objectToRetrieve);
        }

        var comp = obj.GetComponent<IPoolable>();
        comp.OnRetrieve();
        return obj;
	}

    private GameObject GetObject(Poolables objectToRetrieve) {
        return ObjectPools[objectToRetrieve].FirstOrDefault(o => !o.activeInHierarchy);
    }

    private GameObject CreateNewObject(Poolables objectToAdd)
    {
        var ObjectPoolData = ObjectPoolsData.FirstOrDefault(po => po.name == objectToAdd);
        if (ObjectPoolData == null) throw new Exception($"{objectToAdd} is not setup for pooling");

        var obj = Instantiate(ObjectPoolData.prefab);
        obj.SetActive(false);
        ObjectPools[objectToAdd].Add(obj);

        if (obj == null)
        {
            throw new Exception("Fuckyou");
        }
        return obj;
    }

    //getActive items
    //@which - string that tell the function which key to pick from in the dictionary @pooledObjects
    //returns a list of all the active items in the original list in @pooledObjects[which]
    public IEnumerable<GameObject> GetAllActiveObjects(Poolables which) {
        return ObjectPools[which].Where(o => o.activeInHierarchy).ToList();
    }
}

[Serializable]
public class ObjectPoolData {
	public Poolables name;
	public GameObject prefab;
	public int poolSize;
}
