﻿using Assets.Root.Player;
using System.Collections;
using UnityEngine;

public class CameraLock : MonoBehaviour
{
    [Header("Reference")]
    [SerializeField] private Camera playerCamera;
    [SerializeField] private Camera menuCamera;
    [SerializeField] private PlayerMovement player;

    [Header("CameraShake")]
    [SerializeField] private float shakeIntensity = 0.2f;
    [SerializeField] private float shakeDuration = 0.2f;

    [Header("Properties")]
    [SerializeField] private float baseFov;
    [SerializeField] private Vector3 FollowOffset;
    private bool IsShaking { get; set; }

    private bool InGameCameraActive { get; set; }

    void Update()
    {
        ChangeFov();
        MoveToTarget();
    }

    public void Shake()
    {
        if (IsShaking) return;

        IsShaking = true;
        StartCoroutine(MakeShake());
    }

    public IEnumerator MakeShake()
    {
        var originalPosition = playerCamera.transform.localPosition;
        var time = 0f;
        while (time < shakeDuration)
        {
            var shakeX = Random.Range(-1f, 1f) * shakeIntensity;
            var shakeY = Random.Range(-1f, 1f) * shakeIntensity;

            playerCamera.transform.localPosition = new Vector3(shakeX, shakeY, originalPosition.z);
            time += Time.deltaTime;

            yield return null;
        }

        playerCamera.transform.localPosition = originalPosition;
        IsShaking = false;
    }

    private void MoveToTarget()
    {
        var targetPosition = player.transform.position + FollowOffset;
        SmoothPlayerCamera(targetPosition);
    }

    private void SmoothPlayerCamera(Vector3 targetPosition)
    {
        float smooth = 0.05f; //0.01 - super smooth, 1 - super sharp 
        
        var camPos = playerCamera.transform.position;
        //var camRot = playerCamera.transform.rotation;

        //var lookAt = player.transform.position - camPos;
        //var lookAtRotation = Quaternion.LookRotation(lookAt, Vector3.up);

        //playerCamera.transform.rotation = Quaternion.Lerp(camRot, lookAtRotation, smooth);
        playerCamera.transform.position = Vector3.Lerp(camPos, targetPosition, smooth);
    }

    private void ChangeFov()
    {
        var fovMod = (1 + player.CurrentHaste / 4);
        var newFov = fovMod * baseFov;
        playerCamera.fieldOfView = Mathf.Lerp(playerCamera.fieldOfView, newFov, 0.1f);
    }

    public void ShowMenuCam()
    {
        InGameCameraActive = false;
        UpdateCamView();
    }

    public void ShowPlayerCam()
    {
        InGameCameraActive = true;
        UpdateCamView();
    }

    private void UpdateCamView()
    {
        playerCamera.gameObject.SetActive(InGameCameraActive);
        menuCamera.gameObject.SetActive(!InGameCameraActive);
    }

    public void SetBackgroundColor(Color color)
    {
        playerCamera.backgroundColor = color;
        menuCamera.backgroundColor = color;
    }
}
