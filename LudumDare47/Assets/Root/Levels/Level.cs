﻿using Assets.Root.Levels;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "Levels", menuName = "ScriptableObjects/Level", order = 1)]
public class Level : ScriptableObject
{
    public int distance;
    public List<LevelPipe> pipes;

    public int TotalWeight => pipes.Sum(p => p.Chance);
    public IEnumerable<LevelPipe> WeightedPipeList() {
        var weightedPipeList = new List<LevelPipe>();
        foreach (var levelPipe in pipes)
        {
            weightedPipeList.AddRange(Enumerable.Repeat(levelPipe, levelPipe.Chance));
        }

        return weightedPipeList;
    }
}


