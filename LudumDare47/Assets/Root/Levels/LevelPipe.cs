﻿using Assets.Root.Pooler;
using System;

namespace Assets.Root.Levels
{
    [Serializable]
    public class LevelPipe
    {
        public Poolables Pipe;
        public int Chance;
        public bool CanRepeat;
    }
}
