﻿using Assets.Root.Player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace asd
{
    public class TrailController : MonoBehaviour
    {
        [SerializeField] private ParticleSystem ps;
        [SerializeField] private PlayerMovement playerMovement;

        private void Update()
        {
            var zeroToTwo = playerMovement.CurrentHaste + 1;
            var zeroToFourty = zeroToTwo * 10;

            var emission = ps.emission;
            emission.rateOverTime = zeroToFourty;
        }
    }
}