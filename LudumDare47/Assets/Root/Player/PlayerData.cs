﻿using Assets.Root.Audio;
using Assets.Root.PowerUp;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Assets.Root.Player
{
    public class PlayerData : MonoBehaviour
    {
        [SerializeField] private AudioManager audioManager;
        [SerializeField] private PlayerFlasher playerFlasher;

        public int Level { get; set; } = 0;
        public bool IsFlashing { get; set; }

        public const string Tag = "Player";

        public bool IsInGame { get; private set; }

        // in game temp values
        public float DistanceTraveled { get; private set; }
        public int Coins { get; private set; }
        public float Health { get; private set; }

        // permanent values
        public float LongestDistanceTraveled { get; private set; }
        public int Points { get; private set; }

        // permanent powerups
        public int MaxJumpCount { get; private set; } = 1;

        public int MaxHealth { get; private set; } = 1;

        public float BaseSpeedMultiplier { get; set; } = 1;

        // temp powerups
        public float CurrentSpeedMultiplier
        {
            get
            {
                if (tempSpeedBoostActivated)
                {
                    return BaseSpeedMultiplier * 2;
                }
                return BaseSpeedMultiplier;
            }
        }

        private bool tempSpeedBoostActivated = false;
        private float speedMultiplierRemainingTime;

        private Dictionary<int, PowerUpDefinition> powerUps = new Dictionary<int, PowerUpDefinition>();

        private void Update()
        {
            if (speedMultiplierRemainingTime > 0)
            {
                speedMultiplierRemainingTime -= Time.deltaTime;
                speedMultiplierRemainingTime = Mathf.Clamp(speedMultiplierRemainingTime, 0, float.MaxValue);

                if (speedMultiplierRemainingTime == 0)
                {
                    tempSpeedBoostActivated = false;
                }
            }
        }

        public void AddDistanceTraveled(float distance)
        {
            if (IsInGame)
            {
                DistanceTraveled += distance;
            }
        }

        public void ResetPlayerTempValues()
        {
            IsFlashing = false;
            playerFlasher.NoFlash();
            DistanceTraveled = 0;
            Coins = 0;
            Health = MaxHealth;
        }

        public void TakeHit()
        {
            Health--;
            if (Health == 0)
            {
                if (DistanceTraveled > LongestDistanceTraveled)
                {
                    LongestDistanceTraveled = DistanceTraveled;
                }
            }
        }

        public void ActivateSpeedPowerUp()
        {
            tempSpeedBoostActivated = true;
            speedMultiplierRemainingTime = 10;
            audioManager.PlayNow(KnownedTrackCombinaisions.Acceleration);
        }

        public void HealthOne()
        {
            if (Health < MaxHealth)
            {
                Health++;
            }
        }

        public void GiveCoin()
        {
            Coins++;
            audioManager.PlayNow(KnownedTrackCombinaisions.Coin);
        }

        public void GiveMegaCoin()
        {
            Coins += 10;
            audioManager.PlayNow(KnownedTrackCombinaisions.Coin);
        }

        public void GivePoints(int points)
        {
            Points += points;
        }

        public void RemoveFromGame()
        {
            audioManager.StopNow(KnownedTrackCombinaisions.SpeedAmbiantSound);
            gameObject.SetActive(false);
            IsInGame = false;
        }

        public void PutInGame()
        {
            audioManager.PlayNow(KnownedTrackCombinaisions.SpeedAmbiantSound);
            IsInGame = true;
            gameObject.SetActive(true);
        }

        public bool HasPowerUp(int id)
        {
            return powerUps.ContainsKey(id);
        }

        public bool Buy(PowerUpDefinition powerUp)
        {
            if (Points < powerUp.PointsCost)
            {
                return false;
            }

            if (powerUps.ContainsKey(powerUp.Id))
            {
                return false;
            }

            Points -= powerUp.PointsCost;
            powerUps[powerUp.Id] = powerUp;

            ComputePowerUpEffects();
            return true;
        }

        private void ComputePowerUpEffects()
        {
            MaxJumpCount = 1;
            MaxHealth = 1;
            BaseSpeedMultiplier = 1;

            var powerUpsGroups = powerUps.GroupBy(p => p.Value.Name);
            foreach (var powerUpsGroup in powerUpsGroups)
            {
                ComputePowerUpGroupEffects(powerUpsGroup.Select(g => g.Value));
            }
        }

        private void ComputePowerUpGroupEffects(IEnumerable<PowerUpDefinition> powerUpsGroup)
        {
            var maxLevel = powerUpsGroup.Select(g => g.Level).Max();
            var maxPowerUpForGroup = powerUpsGroup.First(g => g.Level == maxLevel);

            MaxJumpCount += maxPowerUpForGroup.IncrementBaseJumpCount;
            MaxHealth += maxPowerUpForGroup.IncrementBaseMaxHeart;
            BaseSpeedMultiplier += maxPowerUpForGroup.IncrementBaseSpeedMultiplier;
        }
    }
}
