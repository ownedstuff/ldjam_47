﻿using Assets.Root.Audio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Root.Player
{
    public class PlayerMovementData
    {
        public bool IsGrounded { get; set; }

        public float CurrentHaste { get; set; } = 1;

        public KnownedTrackCombinaisions LastFrontBackSoundPlayed { get; set; } = KnownedTrackCombinaisions.None;

        public Vector3 GravVelocity { get; set; }

        public float CurrentX { get; set; }

        public float CurrentSideAngle { get; set; }

        public float CurrentFrontBackAngle { get; set; }

        public int CurrentJumpCount { get; set; }
    }
}
