﻿using Assets.Root.Player;
using System.Collections;
using UnityEngine;

public class PlayerFlasher : MonoBehaviour
{
    [Header("References")]
    [SerializeField] private CameraLock _camera;
    [SerializeField] private Renderer _renderer;


    [Header("Properties")]
    [SerializeField] private int numberOfFlashes = 5;

    private PlayerData _data;
    private Color _baseColor = Color.white;


    private void Start()
    {
        _data = GetComponent<PlayerData>();
    }

    public void Flash()
    {
        if (_data.IsFlashing) return;

        _data.IsFlashing = true;
        _camera.Shake();
        StartCoroutine(MakeFlash());
    }

    public void NoFlash() 
    {
        _renderer.material.color = _baseColor;
    }

    private IEnumerator MakeFlash()
    {
        for (int i = 0; i < numberOfFlashes; i++)
        {
            _renderer.material.color = new Color(0.9568f, 0.1921f, 0.0745f);
            yield return new WaitForSeconds(.1f);
            _renderer.material.color = _baseColor;
            yield return new WaitForSeconds(.1f);
        }
        _data.IsFlashing = false;
    }
}
