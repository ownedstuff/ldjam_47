﻿using Assets.Root.Audio;
using UnityEngine;

namespace Assets.Root.Player
{
    public class PlayerMovement : MonoBehaviour
    {
        [Header("References")]
        [SerializeField] private CharacterController controller;
        [SerializeField] private AudioManager audioManager;
        [SerializeField] private PlayerData playerData;
        [SerializeField] private PlayerFlasher flasher;

        [Header("Collision detection")]
        [SerializeField] private Transform[] groudChecks;
        [SerializeField] private float groundDistance = 0.15f;
        [SerializeField] private LayerMask groundMask;
        [SerializeField] private Transform hurtPoint;
        [SerializeField] private float obstacleDistance = 0.15f;
        [SerializeField] private LayerMask obstacleMask;

        [Header("Properties")]
        [SerializeField] private float jumpForce = 20f;
        [SerializeField] private float speed = 30;
        [SerializeField] private float gravity = -30f;
        [SerializeField] private bool lockZPos = true;

        private PlayerMovementData data = new PlayerMovementData();

        private float timeSinceLastJumpSound = 0;
        private float jumpSoundRepeatLimit = 0.1f;

        private PlayerInputs playerInputs = new PlayerInputs();

        public float CurrentHaste => data.CurrentHaste;

        void Update()
        {
            UpdatePlayerInputs();

            UpdateIsGrounded();
            BlockPlayerAtFloor();

            JumpIfPossible();
            Move(playerInputs.X, playerInputs.Z);
            UpdatePlayerAngle();

            PlayTrackBasedOnHasteMode();

            CalculateGravity();
            LockZPosIfNeeded();
        }

        void FixedUpdate()
        {
            CheckCollisionWithObstacles();
        }

        private void CheckCollisionWithObstacles()
        {
            if (playerData.IsFlashing) return;

            var hasCollidedWithObstacle = Physics.CheckSphere(hurtPoint.position, obstacleDistance, obstacleMask);
            if (hasCollidedWithObstacle)
            {
                HitObstacle();
            }
        }

        private void HitObstacle()
        {
            playerData.TakeHit();
            audioManager.PlayNow(KnownedTrackCombinaisions.Hurt);
            flasher.Flash();
        }

        private void PlayTrackBasedOnHasteMode()
        {
            UpdateAmbiantSoundPitch();
            PlaySoundForAccelerationOrDeceleration();
        }

        private void PlaySoundForAccelerationOrDeceleration()
        {
            if (playerInputs.FrontButtonIsDown && data.LastFrontBackSoundPlayed != KnownedTrackCombinaisions.Acceleration)
            {
                audioManager.PlayNow(KnownedTrackCombinaisions.Acceleration);
            }
            else if (playerInputs.BackButtonIsDown && data.LastFrontBackSoundPlayed != KnownedTrackCombinaisions.Deceleration)
            {
                audioManager.PlayNow(KnownedTrackCombinaisions.Deceleration);
            }
        }

        private void UpdateAmbiantSoundPitch()
        {
            var pitch = ((data.CurrentHaste + 1f) * 0.5f) + 0.5f;
            audioManager.ChangePlayingTrackPitch(KnownedTrackCombinaisions.SpeedAmbiantSound, pitch);
        }

        private void LockZPosIfNeeded()
        {
            if (lockZPos)
            {
                var pos = transform.position;
                pos.z = 0;
                transform.position = pos;
            }
        }

        private void UpdatePlayerInputs()
        {
            playerInputs.X = Input.GetAxis("Horizontal");
            playerInputs.Z = Input.GetAxis("Vertical");

            playerInputs.BackButtonIsDown = Input.GetKeyDown(KeyCode.DownArrow) | Input.GetKeyDown(KeyCode.S);
            playerInputs.FrontButtonIsDown = Input.GetKeyDown(KeyCode.UpArrow) | Input.GetKeyDown(KeyCode.W);

            playerInputs.LastJumpState = playerInputs.Jump;
            playerInputs.Jump = Input.GetAxis("Jump") > 0;
        }

        private void UpdateIsGrounded()
        {
            data.IsGrounded = false;
            foreach (var groudCheck in groudChecks)
            {
                data.IsGrounded |= Physics.CheckSphere(groudCheck.position, groundDistance, groundMask);
                data.IsGrounded |= Physics.CheckSphere(groudCheck.position, obstacleDistance, obstacleMask);
                if (data.IsGrounded)
                {
                    AddSlowDownFriction();

                    data.CurrentJumpCount = 0;
                    break;
                }
            }
        }

        private void AddSlowDownFriction()
        {
            var currentGrav = data.GravVelocity;
            var velociryX = Mathf.Lerp(currentGrav.x, 0f, 0.1f);
            data.GravVelocity = new Vector3(velociryX, currentGrav.y, currentGrav.z);
        }

        private void BlockPlayerAtFloor()
        {
            if (data.IsGrounded && data.GravVelocity.y < 0)
            {
                var grav = data.GravVelocity;
                grav.y = -2f;
                data.GravVelocity = grav;
            }
        }

        private void JumpIfPossible()
        {
            if (playerInputs.WantToJump() && (data.CurrentJumpCount < playerData.MaxJumpCount || data.IsGrounded))
            {
                Jump();
            }

            timeSinceLastJumpSound += Time.deltaTime;
        }

        private void Jump()
        {
            PlayJumpSound();

            var grav = data.GravVelocity;
            // on grounded jumps, do an angle jump base on pipe angle
            if (data.IsGrounded)
            {
                var target = Vector3.zero + new Vector3(0, 25, 0);
                var toTarget = target - transform.position;
                var toTargetVector = Vector3.ClampMagnitude(toTarget, 1);
                grav = toTargetVector * jumpForce;
            }
            else
            {
                grav.y = jumpForce;
            }
            data.GravVelocity = grav;

            data.CurrentJumpCount++;
        }

        private void PlayJumpSound()
        {
            if (timeSinceLastJumpSound > jumpSoundRepeatLimit)
            {
                audioManager.PlayNow(KnownedTrackCombinaisions.Jump1);
            }
            timeSinceLastJumpSound = 0;
        }

        private void Move(float x, float z)
        {
            UpdateLerpedCurrentX(x);
            UpdateLerpedCurrentHaste(z);

            Vector3 move = transform.right * data.CurrentX;
            controller.Move(move * speed * Time.deltaTime);
        }

        private void UpdatePlayerAngle()
        {
            UpdateLerpedCurrentSideAngle();
            UpdateLerpedCurrentFrontBackAngle();

            controller.transform.rotation = Quaternion.Euler(data.CurrentFrontBackAngle, 0, data.CurrentSideAngle);
        }

        private void UpdateLerpedCurrentSideAngle()
        {
            var ratio = 1 - Mathf.Abs(data.CurrentX);
            var wantedAngle = ratio * 15;
            var rightSideAngle = (15 - wantedAngle) * Mathf.Sign(data.CurrentX);
            data.CurrentSideAngle = Mathf.Lerp(data.CurrentSideAngle, -rightSideAngle, 0.1f);
        }

        private void UpdateLerpedCurrentFrontBackAngle()
        {
            var wantedAngle = CurrentHaste * 15;
            data.CurrentFrontBackAngle = Mathf.Lerp(data.CurrentFrontBackAngle, wantedAngle, 0.1f);
        }

        private void UpdateLerpedCurrentHaste(float z)
        {
            data.CurrentHaste = Mathf.Lerp(CurrentHaste, z, 0.1f);
        }

        private void UpdateLerpedCurrentX(float x)
        {
            data.CurrentX = Mathf.Lerp(data.CurrentX, x, 0.1f);
        }

        private void CalculateGravity()
        {
            var newVelocity = data.GravVelocity;
            newVelocity.y += gravity * Time.deltaTime;
            data.GravVelocity = newVelocity;
            controller.Move(data.GravVelocity * Time.deltaTime);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Laser") {
                HitObstacle();
            }    
        }

        public struct PlayerInputs
        {
            public float X { get; set; }
            public float Z { get; set; }
            public bool Jump { get; set; }
            public bool LastJumpState { get; set; }
            public bool BackButtonIsDown { get; set; }
            public bool FrontButtonIsDown { get; set; }
            public bool WantToJump()
            {
                return Jump && !LastJumpState;
            }
        }
    }
}