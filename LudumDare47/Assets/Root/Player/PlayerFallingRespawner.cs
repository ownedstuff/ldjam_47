﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Root.Player
{
    public class PlayerFallingRespawner : MonoBehaviour
    {
        [SerializeField] private float fallingBottomBoundY = -75;
        [SerializeField] private PlayerData playerData;

        private void FixedUpdate()
        {
            if (transform.position.y < fallingBottomBoundY)
            {
                transform.position = Vector3.zero;
                playerData.TakeHit();
            }
        }
    }
}