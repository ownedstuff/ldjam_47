﻿using Assets.Root.Mood;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MoodManager : MonoBehaviour
{
    [SerializeField] private CameraLock cameraManager;
    [SerializeField] private Material moodMaterial;
    [SerializeField] private float moodDuration = 6;
    [SerializeField] private float moodTransitionDuration = 3;

    private Tuple<Color, Color, Color, Color> currentMood;
    private Tuple<Color, Color, Color, Color> transitionMood;

    private bool inTransitionMode = false;

    private float timeInCurrentMood = 0;

    private int lastIndexToAvoid = 0;

    private List<Tuple<Color, Color, Color, Color>> moods = new List<Tuple<Color, Color, Color, Color>>()
    {
        MoodPallets.BluePartOf12BitPalette,
        MoodPallets.FuzzyFourPalette,
        MoodPallets.Vivid2BitScreamPalette,
        MoodPallets.NickYellowyPalette,
        MoodPallets.AndradeGameboyPalette,
        MoodPallets.NickCheryPalette,
        MoodPallets.AutumnChillPalette,
    };

    void Start()
    {
        RenderSettings.fog = true;
        RenderSettings.fogDensity = 0.01f;

        transitionMood = currentMood = moods.First();
        InsteadUpdateMood(currentMood);
    }

    private void FixedUpdate()
    {
        timeInCurrentMood += Time.deltaTime;

        if (ReadyToSwapMode())
        {
            SwapMode();
        }
        else
        {
            UpdateLerpMood();
        }
    }

    private bool ReadyToSwapMode()
    {
        if (inTransitionMode)
        {
            return timeInCurrentMood > moodTransitionDuration;
        }
        else
        {
            return timeInCurrentMood > moodDuration;
        }
    }

    private void UpdateLerpMood()
    {
        if (inTransitionMode)
        {
            var percent = timeInCurrentMood / moodTransitionDuration;
            percent = Mathf.Clamp(percent, 0, 1);

            var lerpMood = new Tuple<Color, Color, Color, Color>(
                Color.Lerp(currentMood.Item1, transitionMood.Item1, percent),
                Color.Lerp(currentMood.Item2, transitionMood.Item2, percent),
                Color.Lerp(currentMood.Item3, transitionMood.Item3, percent),
                Color.Lerp(currentMood.Item4, transitionMood.Item4, percent)
            );

            InsteadUpdateMood(lerpMood);
        }
    }

    private void SwapMode()
    {
        timeInCurrentMood = 0;
        inTransitionMode = !inTransitionMode;

        if (inTransitionMode)
        {
            UpdateNextMood();
        }
        else
        {
            KeepSameMood();
        }
    }

    private void KeepSameMood()
    {
        currentMood = transitionMood;
    }

    private void UpdateNextMood()
    {
        while (true) // STUCK IN A LOOP !
        {
            var index = UnityEngine.Random.Range(0, moods.Count);
            if (index != lastIndexToAvoid)
            {
                transitionMood = moods[index];
                lastIndexToAvoid = index;
                break; // BREAK THE LOOP
            }
        }
    }

    private void InsteadUpdateMood(Tuple<Color, Color, Color, Color> mood)
    {
        RenderSettings.ambientLight = mood.Item1;
        RenderSettings.ambientEquatorColor = mood.Item2;
        RenderSettings.ambientGroundColor = mood.Item3;

        RenderSettings.fogColor = mood.Item4;
        cameraManager.SetBackgroundColor(mood.Item4);
        
        moodMaterial.color = mood.Item4;
    }
}
