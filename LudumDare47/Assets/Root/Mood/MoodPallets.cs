﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Root.Mood
{
    public static class MoodPallets
    {
        public static Tuple<Color, Color, Color, Color> NickCheryPalette = new Tuple<Color, Color, Color, Color>(
                Color.magenta,
                Color.blue,
                Color.black,
                Color.white
            );

        public static Tuple<Color, Color, Color, Color> NickYellowyPalette = new Tuple<Color, Color, Color, Color>(
                Color.red,
                Color.yellow,
                Color.gray,
                Color.blue
            );

        // https://lospec.com/palette-list
        public static Tuple<Color, Color, Color, Color> AutumnChillPalette = new Tuple<Color, Color, Color, Color>(
                GenerateColor(213, 136, 99),
                GenerateColor(194, 58, 115),
                GenerateColor(44, 30, 116),
                GenerateColor(218, 211, 175)
            );

        public static Tuple<Color, Color, Color, Color> AndradeGameboyPalette = new Tuple<Color, Color, Color, Color>(
                GenerateColor(227, 238, 192),
                GenerateColor(174, 186, 137),
                GenerateColor(94, 103, 69),
                GenerateColor(32, 32, 32)
            );

        public static Tuple<Color, Color, Color, Color> Vivid2BitScreamPalette = new Tuple<Color, Color, Color, Color>(
                GenerateColor(202, 245, 50),
                GenerateColor(116, 175, 52),
                GenerateColor(92, 79, 163),
                GenerateColor(86, 29, 23)
            );

        public static Tuple<Color, Color, Color, Color> FuzzyFourPalette = new Tuple<Color, Color, Color, Color>(
                GenerateColor(0, 250, 172),
                GenerateColor(48, 35, 135),
                GenerateColor(255, 55, 150),
                GenerateColor(255, 253, 175)
            );

        public static Tuple<Color, Color, Color, Color> BluePartOf12BitPalette = new Tuple<Color, Color, Color, Color>(
                GenerateColor(153, 221, 255),
                GenerateColor(119, 170, 204),
                GenerateColor(85, 119, 153),
                GenerateColor(85, 85, 119)
            );

        private static Color GenerateColor(float r, float g, float b)
        {
            return new Color(r / 255f, g / 255, b / 255);
        }
    }
}
